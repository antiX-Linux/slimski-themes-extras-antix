####---------------------------------####
####  CUSTOM CONFIGURATION FOR antiX ####
####---------------------------------####

## Path, X server, and arguments (if needed) ##
:::    Note: -xauth $authfile is automatically appended
default_path        /usr/local/bin:/usr/bin:/usr/local/games:/usr/games
default_xserver     /usr/bin/X11/X
xserver_arguments  -nolisten tcp

## Full path to the xauth binary (tip: the xauth command has a manpage)
xauth_path     /usr/bin/X11/xauth
## Xauth file for server (created at runtime)
authfile           /var/run/slimski.auth

## Custom commands for systemhalt, login, etc. on antiX
##
##  SECURITY NOTE: the antiX as-shipped sudoers policy
##                 adds all user account into sudoers group
##                 AND grants ( via /etc/sudoers.d/antixers )
##                    %users ALL=(root) NOPASSWD: /usr/sbin/halt
##                    %users ALL=(root) NOPASSWD: /usr/sbin/poweroff
##                    %users ALL=(root) NOPASSWD: /usr/sbin/reboot
## so consider leaving blank these to-be-exposed-via-login-screen commands
systemhalt_enabled  true
systemhalt_cmd      /usr/local/bin/persist-config --shutdown --command halt

reboot_enabled      true
reboot_cmd          /usr/local/bin/persist-config --shutdown --command reboot

## if popfirst_cmd is non-blank (slimski started with -k 
## commandline option and autologin is set false)
## slimski will launch the specified command//program immediately
## after painting the login panel.
## Bear in mind that no window manager is running.
## A fullscreen program will not have a window "titlebar".
#popfirst_cmd   /usr/bin/bash -c '/usr/local/bin/yad --center --color'
#popfirst_cmd   /usr/bin/bash -c 'xvbkd'

exit_enabled        true

## atlogin_cmd is the commandstring to be executed after
## a succesful login.
## You can, optionally, instruct slimski to pass
## the %sessiontype and %theme variable namestrings as commandline options.
atlogin_cmd   setsid /usr/local/bin/desktop-session %sessiontype

## Commands executed when starting and exiting a session.
## They can be used for registering a X11 session with sessreg
##        ( aka  /usr/bin/sessreg )
## You can, optionally, instruct slimski to pass the %username variable
sessionstart_cmd    /usr/bin/sessreg -a -l $DISPLAY %username
sessionstop_cmd     /usr/bin/sessreg -d -l $DISPLAY %username

## Start in daemon mode
## note: This option, if set true here, can be temporarily overriden
##       at runtime by the command line options
##       "-d" and "-nodaemon",
##
##  slimski is already setup to launch as daemon via an initscript
#daemonmode_enabled       true

## Available sessiontypes
## (first one listed shall be the default selection).
## THE VALUE FOR sessiontypes SHOULD BE COMMA-SEPARATED, NO SPACES
sessiontypes fluxbox,herbstluftwm,icewm,jwm,rox-fluxbox,rox-icewm,rox-jwm,zzz-fluxbox,zzz-icewm,zzz-jwm,minimal-fluxbox,minimal-icewm,minimal-jwm

## You can also set here the default sessiontype
default_sessiontype     rox-icewm

## You can also set a path for slimski to get the sessiontypes
##    If you (uncomment below, and) supply a non-blank pathstring,
##    xsessionsdir lookup will be performed
##    (and the "sessiontypes" conf option will be suppressed).
## Remember to change the atlogin_cmd if not using desktop-session
## and only want to use the commands inside the .desktop in xsessiondir
#xsessionsdir     /usr/share/xsessions

## CUSTOM COMMAND to be executed when pressing F11
#F11_cmd      import -window root /slimski.png
##                ^--- for example, if IMAGEMAGICK IS INSTALLED
F11_cmd      scrot    /tmp/slimski.png

### Security note regarding custom commands:
##  When choosing a command (or none), bear in mind that this command
##  will be executed with elevated priviledges.
##  If the command is not properly thought out, you could
##  overwrite and remove files and folders /may brake your system)

##  F5_cmd  provides ANOTHER optional customizable command,
##  will be executed if user presses the F5 key while viewing the slimski screen
F5_cmd       "bash -c 'yad --color'"

##    Keybinds can now, optionally, be assigned to each of the F2--F12 keys.

## welcome message, displayed within the bounds of panel.
## Available variables: %host, %domain
welcome_msg         Welcome to %host

## F1 message, displayed on the root window (aka background)
F1_msg         (Press F1 to change sessiontype)

## Prepended to the sessiontype name each time F1 is pressed
sessiontype_msg        Sessiontype:

## shutdown(aka systemhalt) / reboot messages
systemhalt_msg   The system is shutting down...
reboot_msg     The system is rebooting...

## Immediately focus the password field (requires a default_user).
passwdfocus_enabled      true

##   LOGIN TO GRAPHICAL XSESSION USING ROOTUSER ACCOUNT IS FORBIDDEN.
##   Understand that this is a non-configurable stipulation.
##   This is the message you will see when trying to login as root:
forbidden_msg   ROOTUSER ACCOUNT LOGIN IS FORBIDDEN

### OPTIONS ###
##  numlock_enabled will toggle numlock as soon as the panel appears
##  Set your theme with current_theme
##       (searches for it in /usr/share/slimski/themes)
##  Autologin will work if the default_user is set (and exists)
##  if default_user is blank, you need to write the username to login
numlock_enabled   true
current_theme     antiX
#autologin_enabled false
#default_user      demo

# Note: logfile is already set to /var/log/slimski.log (canot be changed)
